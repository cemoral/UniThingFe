package com.csemprojects.unithing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.csemprojects.unithing.entity.Location;
import com.csemprojects.unithing.entity.Post;
import com.csemprojects.unithing.service.HttpRequestService;

/**
 * Created by cemor on 2018-04-25.
 */

public class NewPostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_post);
        Button btnSend = findViewById(R.id.btnSend);
        final EditText txtPost = findViewById(R.id.txtPost);

        btnSend.setOnClickListener((view) -> {
            Post p = new Post();
            p.setText(txtPost.getText().toString());
            Location loc = new Location();
            loc.setId("1");
            loc.setName("TEST");
            p.setLocation(loc);
            HttpRequestService service = new HttpRequestService((result) -> {
                Toast.makeText(getApplicationContext(), "Post Created", Toast.LENGTH_LONG).show();
                setResult(31);
                finish();
            });
            service.execute("newPost", p);
        });
    }
}
