package com.csemprojects.unithing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.csemprojects.unithing.entity.Comment;
import com.csemprojects.unithing.service.HttpRequestService;

public class NewCommentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_post);
        Button btnSend = findViewById(R.id.btnSend);

        btnSend.setOnClickListener((view) -> {
            Comment comment = new Comment();
            EditText txtPost = findViewById(R.id.txtPost);
            comment.setText(txtPost.getText().toString());
            comment.setPostId(getIntent().getSerializableExtra("postId").toString());
            HttpRequestService service = new HttpRequestService((result) ->
            {
                Toast.makeText(getApplicationContext(), "Comment Created", Toast.LENGTH_LONG).show();
                finish();
            });
            service.execute("newComment", comment);
        });
    }

}
