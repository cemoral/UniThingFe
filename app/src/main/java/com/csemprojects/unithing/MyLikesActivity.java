package com.csemprojects.unithing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import com.csemprojects.unithing.adapter.PostAdapter;
import com.csemprojects.unithing.entity.Location;
import com.csemprojects.unithing.entity.Post;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Sem on 08.05.2018.
 */

public class MyLikesActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylikes);
        List<Post> pst = new ArrayList<>();
        Location loc = new Location();
        loc.setId("1");
        loc.setName("TEST");
        for (int i = 0; i < 30; i++) {
            Post p = new Post();
            p.setLocation(loc);
            p.setText("Home" + i);
            p.setDownVote(30 - i);
            p.setUpVote(i);
            p.setId(String.valueOf(i + 1));
            Calendar d = Calendar.getInstance();
            d.add(Calendar.DATE, -i);
            pst.add(p);
        }
        PostAdapter adapter = new PostAdapter(this, pst);
        ListView lst = findViewById(R.id.lstMyLikes);
        lst.setAdapter(adapter);

    }
}
