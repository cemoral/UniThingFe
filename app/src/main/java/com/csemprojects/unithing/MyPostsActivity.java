package com.csemprojects.unithing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import com.csemprojects.unithing.adapter.PostAdapter;
import com.csemprojects.unithing.entity.Post;
import com.csemprojects.unithing.service.HttpRequestService;

import java.util.List;

/**
 * Created by Sem on 08.05.2018.
 */

public class MyPostsActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myposts);
        try {
            HttpRequestService service = new HttpRequestService((result) -> {
                PostAdapter adapter = new PostAdapter(this, (List<Post>) result);
                ListView lst = findViewById(R.id.lstMyPosts);
                lst.setAdapter(adapter);
            });
            service.execute("myPosts").get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
