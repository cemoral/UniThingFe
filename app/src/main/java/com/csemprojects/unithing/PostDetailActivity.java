package com.csemprojects.unithing;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.csemprojects.unithing.adapter.CommentAdapter;
import com.csemprojects.unithing.adapter.PostAdapter;
import com.csemprojects.unithing.entity.Comment;
import com.csemprojects.unithing.entity.Post;
import com.csemprojects.unithing.service.HttpRequestService;

import java.util.List;

/**
 * Created by cemor on 2018-05-07.
 */

public class PostDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postdetail);

        swipeRefreshLayout = findViewById(R.id.postDetailRefresher);
        swipeRefreshLayout.setOnRefreshListener(this);

        final Post post = (Post) getIntent().getSerializableExtra("postId");
        getCommentsByPost(post.getId());

        TextView tvDetail = findViewById(R.id.tvPost);
        Button upVote = findViewById(R.id.Up);
        Button downVote = findViewById(R.id.Down);
        Button addComment = findViewById(R.id.addComment);
        upVote.setText(String.valueOf(post.getUpVote()));
        downVote.setText(String.valueOf(post.getDownVote()));
        tvDetail.setText(post.getText());
        tvDetail.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPostOne));

        upVote.setOnClickListener((view) -> {
            String count = String.valueOf(Integer.parseInt(upVote.getText().toString()) + 1);
            upVote.setText(count);
            new HttpRequestService().execute("upVote", String.valueOf(post.getId()));
        });
        downVote.setOnClickListener((view) -> {
            String count = String.valueOf(Integer.parseInt(downVote.getText().toString()) + 1);
            downVote.setText(count);
            new HttpRequestService().execute("downVote", String.valueOf(post.getId()));
        });
        addComment.setOnClickListener((view) -> {
            Intent intent = new Intent(getBaseContext(), NewCommentActivity.class);
            intent.putExtra("postId", post.getId());
            startActivity(intent);
        });
    }

    @Override
    public void onRefresh() {
        final Post post = (Post) getIntent().getSerializableExtra("postId");
        getCommentsByPost(post.getId());
    }

    private void getCommentsByPost(String postId) {
        HttpRequestService service = new HttpRequestService((response) -> {
            if (null != response) {
                CommentAdapter adapter = new CommentAdapter(this, (List<Comment>) response, getIntent().getIntExtra("postColor",
                        1));
                ListView listView = findViewById(R.id.lstComment);
                listView.setAdapter(adapter);
            }
            swipeRefreshLayout.setRefreshing(false);
        });
        service.execute("getCommentsByPost", postId);
    }
}
