package com.csemprojects.unithing.service;

import android.os.AsyncTask;
import com.csemprojects.unithing.entity.Auth;
import com.csemprojects.unithing.entity.Comment;
import com.csemprojects.unithing.entity.Post;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cemor on 2018-04-06.
 */

public class HttpRequestService extends AsyncTask<Object, Void, List<?>> {

    //private static final String Url = "http://unithing.serveo.net:1030";
    private final String Url = "http://192.168.1.11:1010";

    private HttpHeaders headers;

    private MappingJackson2HttpMessageConverter converter;

    public interface AsyncResponse {
        void processFinish(List<?> output);
    }

    private AsyncResponse delegate;

    public HttpRequestService(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    public HttpRequestService() {
    }

    @Override
    protected List<?> doInBackground(Object... objects) {

        try {
            if (null == headers || null == converter)
                getToken();
            //this.((String) objects[0], Object[].class).invoke(Arrays.stream(objects).skip(1));
            switch ((String) objects[0]) {
                case "getPosts":
                    return getPosts(objects[1].toString(), objects[2].toString());
                case "upVote":
                    return setUpVote(String.valueOf(objects[1]));
                case "downVote":
                    return setDownVote(String.valueOf(objects[1]));
                case "newPost":
                    return newPost((Post) objects[1]);
                case "myPosts":
                    return myPosts();
                case "getCommentsByPost":
                    return getCommentsByPost(String.valueOf(objects[1]));
                case "newComment":
                    return newComment((Comment) objects[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    protected void onPostExecute(List<?> objects) {
        if(delegate != null)
        delegate.processFinish(objects);
    }

    private void getToken() {
        RestTemplate restTemplate = new RestTemplate(true);
        JSONObject body = new JSONObject();
        try {
            body.put("username", "user");
            body.put("password", "user");
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), reqHeaders);
        HttpEntity<Auth> token = restTemplate.exchange(Url + "/auth/signin", HttpMethod.POST, request, Auth.class);
        reqHeaders.set("Authorization", "Bearer " + token.getBody().getToken());
        headers = reqHeaders;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(mapper);
    }

    private List<Post> getPosts(String orderParam, String locationId) {
        String uri;
        if (orderParam.equals("byDate"))
            uri = "/api/posts/byDate/" + locationId;
        else
            uri = "/api/posts/byRelevance/" + locationId;
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity httpEntity = new HttpEntity(headers);
        restTemplate.setMessageConverters(Collections.singletonList(converter));
        HttpEntity<Post[]> response = restTemplate.exchange(Url + uri, HttpMethod.GET, httpEntity, Post[].class);
        return Arrays.stream(response.getBody()).collect(Collectors.toList());
    }

    private List<Post> setUpVote(String postId) {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity httpEntity = new HttpEntity(headers);
        restTemplate.postForObject(Url + "/api/posts/upVote/" + postId, httpEntity, ResponseEntity.class);
        return null;
    }

    private List<Post> setDownVote(String postId) {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity request = new HttpEntity(headers);
        restTemplate.postForObject(Url + "/api/posts/downVote/" + postId, request, ResponseEntity.class);
        return null;
    }

    private List<Post> newPost(Post p) {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity<Post> request = new HttpEntity<>(p, headers);
        restTemplate.exchange(Url + "/api/posts", HttpMethod.POST, request, ResponseEntity.class);
        return null;
    }

    private List<Post> myPosts() {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity<Post> request = new HttpEntity<>(headers);
        restTemplate.setMessageConverters(Collections.singletonList(converter));
        HttpEntity<Post[]> response = restTemplate.exchange(Url + "/api/posts/user", HttpMethod.GET, request, Post[].class);
        return Arrays.stream(response.getBody()).collect(Collectors.toList());
    }

    private List<Comment> getCommentsByPost(String postId) {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity request = new HttpEntity<>(headers);
        restTemplate.setMessageConverters(Collections.singletonList(converter));
        HttpEntity<Comment[]> response = restTemplate.exchange(Url + "/api/comments/post/" + postId, HttpMethod.GET, request, Comment[].class);
        return Arrays.stream(response.getBody()).collect(Collectors.toList());
    }

    private List<?> newComment(Comment comment) {
        RestTemplate restTemplate = new RestTemplate(true);
        HttpEntity<Comment> request = new HttpEntity<>(comment, headers);
        restTemplate.exchange(Url + "/api/comments", HttpMethod.POST, request, ResponseEntity.class);
        return Collections.EMPTY_LIST;
    }

}
