package com.csemprojects.unithing.service;

import com.csemprojects.unithing.entity.Auth;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class RestClient {

    private static final String BASE_URL = "http://192.168.43.50:1010";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Authorization", getToken());
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Authorization", getToken());
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    private static String getToken() {

        AsyncHttpClient tClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("username", "user");
        requestParams.put("password", "user");
        tClient.post(getAbsoluteUrl("/auth/signin"), requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Object object = response;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
        /*JSONObject body = new JSONObject();
        RestTemplate restTemplate = new RestTemplate(true);
        try {
            body.put("username", "user");
            body.put("password", "user");
        } catch (Exception e) {
            e.getMessage();
        }
        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), reqHeaders);
        HttpEntity<Auth> token = restTemplate.exchange(BASE_URL + "/auth/signin", HttpMethod.POST, request, Auth.class);
        return "Bearer " + token.getBody().getToken();*/
        return "";
    }

}
