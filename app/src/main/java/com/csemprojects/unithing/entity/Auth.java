package com.csemprojects.unithing.entity;

/**
 * Created by cemor on 2018-03-07.
 */

public class Auth {

    private String token;

    private String username;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
