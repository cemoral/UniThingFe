package com.csemprojects.unithing.entity;

import java.io.Serializable;

public class Location implements Serializable {

    private static final long serialVersionUID = 7371158896687288934L;

    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
