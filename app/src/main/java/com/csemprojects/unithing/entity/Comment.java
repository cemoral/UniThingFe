package com.csemprojects.unithing.entity;

import java.io.Serializable;

/**
 * Created by cemor on 2018-05-07.
 */

public class Comment implements Serializable {

    private static final long serialVersionUID = 5180496386351133199L;

    private String id;

    private String text;

    private String postId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}

