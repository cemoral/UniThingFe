package com.csemprojects.unithing.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.csemprojects.unithing.PostDetailActivity;
import com.csemprojects.unithing.R;
import com.csemprojects.unithing.entity.Post;
import com.csemprojects.unithing.service.HttpRequestService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cemor on 2018-04-16.
 */

public class PostAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private List<Post> Posts;

    public PostAdapter(Activity a, List<Post> p) {
        Posts = p;
        inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Posts.size();
    }

    @Override
    public Object getItem(int i) {
        return Posts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.postlist, null);

        final TextView post = vi.findViewById(R.id.tvPost);
        final Button upVote = vi.findViewById(R.id.Up);
        final Button downVote = vi.findViewById(R.id.Down);
        post.setText(Posts.get(position).getText());
        upVote.setText(String.valueOf(Posts.get(position).getUpVote()));
        downVote.setText(String.valueOf(Posts.get(position).getDownVote()));
        vi.setTag(Posts.get(position).getId());
        vi.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), PostDetailActivity.class);
            intent.putExtra("postId", Posts.get(position));
            intent.putExtra("postColor", ((ColorDrawable) view.getBackground()).getColor());
            view.getContext().startActivity(intent);
        });
        upVote.setOnClickListener(view -> {
            String count = String.valueOf(Integer.parseInt(upVote.getText().toString()) + 1);
            upVote.setText(count);
            new HttpRequestService().execute("upVote", String.valueOf(Posts.get(position).getId()));
        });
        downVote.setOnClickListener(view -> {
            String count = String.valueOf(Integer.parseInt(downVote.getText().toString()) + 1);
            downVote.setText(count);
            new HttpRequestService().execute("downVote", String.valueOf(Posts.get(position).getId()));
        });
        post.setTextColor(ContextCompat.getColor(vi.getContext(), R.color.colorWhite));
        if (getItemId(position) % 4 == 0)
            vi.setBackgroundColor(ContextCompat.getColor(vi.getContext(), R.color.colorPostOne));
        else if (getItemId(position) % 4 == 1)
            vi.setBackgroundColor(ContextCompat.getColor(vi.getContext(), R.color.colorPostTwo));
        else if (getItemId(position) % 4 == 2)
            vi.setBackgroundColor(ContextCompat.getColor(vi.getContext(), R.color.colorPostThree));
        else if (getItemId(position) % 4 == 3)
            vi.setBackgroundColor(ContextCompat.getColor(vi.getContext(), R.color.colorPostFour));
        return vi;
    }
}
