package com.csemprojects.unithing.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.csemprojects.unithing.PostDetailActivity;
import com.csemprojects.unithing.R;
import com.csemprojects.unithing.entity.Comment;

import java.util.List;

/**
 * Created by cemor on 2018-05-07.
 */

public class CommentAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private List<Comment> comments;
    private int color;

    public CommentAdapter(Activity activity, List<Comment> c, int color) {
        comments = c;
        this.color = color;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int i) {
        return comments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(final int position, final View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.commentlist, null);

        final TextView post = vi.findViewById(R.id.tvComment);
        post.setText(comments.get(position).getText());
        vi.setTag(comments.get(position).getId());
        post.setOnClickListener((view) -> {
            Intent intent = new Intent(convertView.getContext(), PostDetailActivity.class);
            convertView.getContext().startActivity(intent);
        });
        post.setTextColor(ContextCompat.getColor(vi.getContext(), R.color.colorWhite));
        vi.setBackgroundColor(color);
        return vi;
    }
}
