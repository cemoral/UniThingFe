package com.csemprojects.unithing;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.ListView;
import android.widget.Toast;
import com.csemprojects.unithing.adapter.PostAdapter;
import com.csemprojects.unithing.adapter.SectionsPagerAdapter;
import com.csemprojects.unithing.entity.Post;
import com.csemprojects.unithing.service.HttpRequestService;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(0);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), NewPostActivity.class);
            startActivityForResult(intent, 30);
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sortByRelevance) {
            PlaceholderFragment.OrderByParam = "byRelevance";
        } else if (id == R.id.sortByDate) {
            PlaceholderFragment.OrderByParam = "byDate";
        }
        getPosts();
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        private static String OrderByParam = "byDate";

        private SwipeRefreshLayout mSwipeRefreshLayout;

        private ListView listView;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                                 final Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            mSwipeRefreshLayout = rootView.findViewById(R.id.swiperefresh);
            mSwipeRefreshLayout.setOnRefreshListener(this::update);
            listView = rootView.findViewById(R.id.lst);
            update();
            return rootView;
        }

        public void update() {
            try {
                Integer pageNumber = getArguments().getInt(ARG_SECTION_NUMBER);
                HttpRequestService service = new HttpRequestService((output) -> {
                    if (null != output) {
                        PostAdapter adapter = new PostAdapter(getActivity(), (List<Post>) output);
                        listView.setAdapter(adapter);
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                });
                service.execute("getPosts", PlaceholderFragment.OrderByParam, pageNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_myPosts) {
            Intent intent = new Intent(getBaseContext(), MyPostsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_myLikes) {
            Intent intent = new Intent(getBaseContext(), MyLikesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_exit) {
            System.exit(1);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 30 && resultCode == 31) {
            getPosts();
        }
    }

    private void getPosts() {
        ListView listView = findViewById(R.id.lst);
        ViewPager mViewPager = findViewById(R.id.container);
        HttpRequestService service = new HttpRequestService((output) -> {
            if (null != output) {
                PostAdapter adapter = new PostAdapter(this, (List<Post>) output);
                listView.setAdapter(adapter);
                Toast.makeText(getApplicationContext(), "Content Updated", Toast.LENGTH_LONG).show();
            }
        });
        service.execute("getPosts", PlaceholderFragment.OrderByParam, mViewPager.getCurrentItem() + 1);
    }
}
